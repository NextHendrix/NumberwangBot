-- NumberwangBot
-- © NextHendrix 2018
{-# LANGUAGE OverloadedStrings, MultiWayIf #-}
module NumberwangBot.Numberwang (numberWang) where

-- is-three.js
numberWang :: Integral a => a -> Bool
numberWang = (==3)
