-- NumberwangBot
-- © NextHendrix 2018
{-# LANGUAGE OverloadedStrings, MultiWayIf #-}
module NumberwangBot.Parse (containsNumber) where

import qualified Data.ByteString.Char8 as B
import Network.IRC

-- 1. Scans line for numbers
-- 2. Compiles numbers into a list (of strings)
-- 3. Chooses one
-- 4. Sends it off to be tested for numberwang worthiness

-- Testing version: Just checks if there's a number in the message
containsNumber :: Message -> Bool
containsNumber msg = containsNumber' ((B.unpack . head . tail) (msg_params msg))
    where
      containsNumber' :: [Char] -> Bool
      containsNumber' [] = False
      containsNumber' (x:xs)
        | x `elem` ("0123456789" :: String) = True
        | otherwise = containsNumber' xs
