-- NumberwangBot
-- © NextHendrix 2018
{-# LANGUAGE OverloadedStrings, MultiWayIf #-}

module Main where

import Network
import Control.Monad (forever)
import System.IO
import Network.IRC
import Data.Foldable
import qualified Data.ByteString.Char8 as B
import System.Random
import NumberwangBot.Numberwang
import NumberwangBot.Parse
import System.Environment

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  putStr "Enter password: "
  pass <- B.pack <$> getLine
  putStr "Conecting..."
  s <- connectTo "finickitively.co.uk" (PortNumber 6667)
  putStrLn "connected."
  B.hPutStrLn s ("PASS " <> pass)
  putStrLn "Sent Password"
  traverse_ (B.hPutStrLn s) initialise
  putStrLn "Initialised"
  putStrLn "Let's Numberwang!"
  forever $ do
    raw <- decode <$> B.hGetLine s
    case raw of
      Nothing -> B.hPutStrLn s "E R R O R   L A D S"
      Just m -> if | msg_command m == "PING" -> ping s m
                   | msg_command m == "PRIVMSG" ->
                       if containsNumber m
                       then do
                         B.putStrLn "Found Number!"
                         n <- randomRIO (0, 10) :: IO Integer
                         print n
                         case numberWang n of
                           True -> do
                             B.putStrLn (showMessage m)
                             B.hPutStrLn s (encode $ ((privmsg . head) (msg_params m)) (getNick m <> ": That's Numberwang!"))
                           False -> B.putStrLn (showMessage m)
                       else
                         B.putStrLn (showMessage m)
                   | otherwise -> do
                       B.putStrLn "No Numbers Found"
                       B.putStrLn (showMessage m)

initialise :: [B.ByteString]
initialise = (encode <$> [ (user "NumberwangBot" "0" "*" "NumberwangBot")
                         , (nick "NumberwangBot")
                         , (joinChan "##NextHendrix")
                         , (joinChan "#chee-fanclub")
                         , (joinChan "#omp-fanclub")
                         ])

ping :: Handle -> Message -> IO ()
ping h msg = do
  B.hPutStrLn h outMsg
  B.hPutStrLn stdout outMsg
  B.hPutStrLn stdout "Sent Ping"
    where outMsg = encode . pong . mconcat . msg_params $ msg

getNick :: Message -> B.ByteString
getNick msg = (\(NickName n _ _) -> n) (unmaybe $ msg_prefix msg)
  where unmaybe = (\(Just a) -> a)
